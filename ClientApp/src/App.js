import React, { Component } from 'react';
//import { Route } from 'react-router';
import {Routes, Route } from "react-router-dom";
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { FetchData } from './components/FetchData';
import { Counter } from './components/Counter';
import './custom.css'

export default class App extends Component {
  static displayName = App.name;

  render () {

   // return(<Counter/>)
    return (
      
      <Layout>
         <Routes>
        <Route exact path='/' element={<Home/>} />
        <Route path='/counter' element={<Counter/>} />
        <Route path='/fetch-data' element={<FetchData/>} />
        </Routes>
      </Layout>
      
    );
  }
}
